from django import forms

class subscribeForm(forms.Form):

    invalid_message={
        'required' : 'Please fill out this field',
        'invalid' : 'Please fill out with valid Input',
    }

    fullname_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
    }

    email_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'abc@example.com',
    }

    password_attrs={
        'type' : 'password',
        'class' : 'todo-form-input',
    }

    fullName = forms.CharField(label="Full Name", required=True, max_length=30, widget=forms.TextInput(attrs = fullname_attrs))
    email = forms.EmailField(label="Email", required=True, max_length=30, widget=forms.TextInput(attrs = email_attrs))
    password = forms.CharField(label="Password", required=True, max_length=30, widget=forms.PasswordInput(attrs = password_attrs))
