var valid = false;
var csrftoken = getCookie('csrftoken');
function getCookie(c_name){
    if(document.cookie.length > 0){
        c_start = document.cookie.indexOf(c_name + "=");
        if(c_start != -1){
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function csrftokenSafeMethod(method){
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function validEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isValid(){
    if(($("#id_email").val()==null || $("#id_email").val()=="") || ($("#id_password").val()==null || $("#id_password").val()=="") || 
    ($("#id_fullName").val()==null || $("#id_fullName").val()=="")){
        return false;
    }
    return true;
}

$(document).ready(function(){
    $.ajaxSetup({
        beforeSend: function(xhr, settings){
            if(!csrftokenSafeMethod(settings.type) && !this.crossDomain){
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

$("form").submit(function(event){
    $.ajax({
        method: "POST",
        url: 'submit/',
        data: {
            'fullName' : $("#id_fullName").val(),
            'email' : $("#id_email").val(),
            'password' : $("#id_password").val(),
        },
        dataType: 'json',
        success: function(msg){
            $( "#dialog" ).dialog();
                $( "#dialog" ).html("<p>"+msg['result']+"</p>");
                $("#id_fullName").val('');
                $("#id_email").val('');
                $("#id_password").val('');
        },
            error: function(msg){
                alert('Email not sent');
        },

    }),
    event.preventDefault();
});

$("#id_fullname").change(function(){
    if(isValid() && valid){
        $("#submit")[0].disabled=false;
    }
    else{
        $("#submit")[0].disabled=true;
    }
});

$("#id_password").change(function(){
    if(isValid() && valid){
        $("#submit")[0].disabled=false;
    }
    else{
        $("#submit")[0].disabled=true;
    }
});

$("#id_email").change(function(){
    email = $(this).val();
    $.ajax({
        method: "POST",
        url: 'validation/',
        data: {
            'email': email
        },
        dataType: 'json',
        success: function(data){
            if(validEmail(email)==false){
                $('#id_email').css("border-color", "red");
                $("#submit")[0].disabled=true;
                $('#waitUp').html('Enter a correct email address');
                valid = false;
            }
            else if (data.isTaken) {
                alert("Email is taken");
                $('#id_email').css("border-color", "red");
                $("#submit")[0].disabled=true;
                $('#waitUp').html('Use other email address');
                valid = false;
            }
            else{
                if(isValid()){
                    $("#submit")[0].disabled=false;	
                }
                valid = true;
                $('#id_email').css("border-color", "white");
                $('#waitUp').html('');
            }
        }
    });
});
});

$(document).ready(function() {
    $.ajax({
        url: "subscriber/",
        dataType: 'json',
        success: function(data){
            console.log(data.subscriber.length)
            $('tbody').html('')
            var stringHTML ='<tr>';
            for(i = 0; i < data.subscriber.length; i++){
                stringHTML +=
                "<th class='align-middle' scope="+"'row'"+">" + (i+1) + "</th>" +
                "<td class='align-middle'>" + data.subscriber[i].fullName +"</td>" + 
                "<td class='align-middle'>" + data.subscriber[i].email + "</td>" +
                "<td class='align-middle' style='text-align:center'>" + '<button id="unsub" onclick="unsubscribe(' + data.subscriber[i].id + ')" type="button" class="btn btn-danger">Unsubscribe</button>' + "</td></tr>";
            }
            $('tbody').append(stringHTML);
        },
    })
});

function unsubscribe(id){
    console.log(id)
    $.ajax({
        url: "unsubscribe/",
        type: "POST",
        data: {
            'id' : id
        },
        success: function(id){
            window.location.reload(true);
        }
    });
}
