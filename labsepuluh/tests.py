from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import displayForm, runForm, emailValidation
from .forms import subscribeForm
from .models import subscribe
from django.db import IntegrityError

class FormTest(TestCase):
    def test_labsepuluh_url_is_exist(self):
        response = Client().get('/labsepuluh/')
        self.assertEqual(response.status_code, 200)

    def test_labsepuluh_using_template(self):
        response = Client().get('/labsepuluh/')
        self.assertTemplateUsed(response, 'labsepuluh.html')

    def test_labsepuluh_url_is_exist2(self):
        response = Client().get('/labsepuluh/validation/')
        self.assertEqual(response.status_code,200)
        response = emailValidation(HttpRequest())
        html = response.content.decode('utf8')
        self.assertIn('false', html)


    def test_post(self):
        fullname = 'test'
        email= 'test@testcase.com'
        password= 'test1234567'
        response_post = Client().post('/labsepuluh/submit/',{ 'fullName': fullname, 'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 200)

    
    def test_post2(self):
        fullname = 'test'
        email= ''
        password='test1234567'
        response_post = Client().get('/labsepuluh/submit/',{ 'fullName': fullname, 'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 200)
        