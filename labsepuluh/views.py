from django.shortcuts import render, redirect
from django.db import IntegrityError
from django.http import HttpResponse
from .models import subscribe
from .forms import subscribeForm
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.db import IntegrityError
import json

def displayForm(request):
    response = {}
    form = subscribeForm(request.POST or None)
    response['form'] = form
    return render(request, 'labsepuluh.html', response)

def runForm(request):
    response = {}
    try:
        if(request.method == "POST"):
             fullName = request.POST.get('fullName', None)
             email = request.POST.get('email', None)
             password = request.POST.get('password', None)
             subscribe.objects.create(fullName=fullName, email=email, password=password)
             return HttpResponse(json.dumps({'result' : 'Thankyou ' + fullName + ' for subscribing!'}))
        else:
            return HttpResponse(json.dumps({'result' : 'fail'}))

    except IntegrityError as e:
        return HttpResponse(json.dumps({'result' : 'fail'}))

@csrf_exempt
def emailValidation(request):
    theEmail = request.POST.get('email', None)
    validation = {
        'isTaken' : subscribe.objects.filter(email=theEmail).exists()
    }
    return JsonResponse(validation)

def subscriber(request):
    subscriber = subscribe.objects.all().values()
    subs = list(subscriber)
    return JsonResponse({'subscriber' : subs})

@csrf_exempt
def unsubscribe(request):
    if(request.method == 'POST'):
        identity = request.POST['id']
        delete = subscribe.objects.filter(id=identity).delete()
        return render(request, 'labsepuluh.html')
