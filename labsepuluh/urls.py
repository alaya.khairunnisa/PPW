from django.contrib import admin
from django.urls import path
from . import views
app_name= 'labsepuluh'
urlpatterns = [
    path('', views.displayForm, name='displayForm'),
	path('submit/', views.runForm, name='runForm'),
	path('validation/', views.emailValidation, name='emailValidation'),
	path('subscriber/', views.subscriber, name='subscriber'),
	path('unsubscribe/', views.unsubscribe, name='unsubscribe'),
]
