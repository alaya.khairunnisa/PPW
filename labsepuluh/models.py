from django.db import models

# Create your models here.
class subscribe(models.Model):
    fullName = models.CharField(max_length=30)
    email = models.EmailField(max_length=30, unique=True)
    password = models.CharField(max_length=30)
