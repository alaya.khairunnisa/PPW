from django.db import models
# Create your models here.
from django.db import models
from django.utils import timezone 
from datetime import datetime, date

class Activity(models.Model):
    day = models.CharField(max_length=10) 
    date = models.DateField()
    time = models.TimeField()
    eventname = models.CharField(max_length=50)
    place = models.CharField(max_length=30) 
    category = models.CharField(max_length=30) 


    def publish(self):
        self.save()

    def __str__(self):
        return self.eventname