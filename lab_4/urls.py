"""websiteproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.first, name = 'first'),
    path('home', views.home, name = 'home'),
    path('profile', views.profile, name = 'profile'),
    path('education', views.education, name = 'education'),
    path('experience', views.experience, name = 'experience'),
    path('personalinformation', views.personalinfo, name = 'personalinformation'),
    path('photos', views.photos, name = 'photos'),
    path('mytwin', views.mytwin, name = 'mytwin'),
    path('guestbook', views.guestbook, name = 'guestbook'),
    path('activity', views.activity, name = 'activity'),
    path('createactivity', views.createactivity, name = 'createactivity'),
    path('delete', views.delete, name = 'delete'),

]