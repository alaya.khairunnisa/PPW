from django import forms
from .models import Activity

class Activity_Form(forms.Form):
    day = forms.CharField(label='day', required=True,
        max_length=10)
    date = forms.DateField(label='date', required=True, widget=forms.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(label='time', required=True, widget=forms.TimeInput(attrs={'type' : 'time'}))
    eventname = forms.CharField(label='eventname', required=True)
    place = forms.CharField(label='place', required=True)
    category = forms.CharField(label='category', required=True,
        max_length=30)