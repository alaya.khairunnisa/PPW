from django.shortcuts import render
from django.http import HttpResponse
from .forms import Activity_Form
from .models import Activity

response={}
# Create your views here.
def first (request):
    return render(request, "Story3(first).html", {})

def home(request):
    return render(request, "Story3(home).html", {})
    
def profile (request):
    return render(request, "Story3(profile).html", {})

def education (request):
    return render(request, "Story3(education).html", {})

def experience (request):
    return render(request, "Story3(experiences).html", {})

def personalinfo (request):
    return render(request, "Story3(personalinfo).html", {})

def photos (request):
    return render(request, "Story3(photos).html", {})

def mytwin (request):
    return render(request, "Story3(mytwin).html", {})

def guestbook (request):
    return render(request, "Story3(guestbook).html", {})

def createactivity (request):
    activities = Activity.objects.all()
    response = {
        "Activities" : activities
    }
    response['sched'] = Activity_Form
    return render(request, 'Story3(createactivity).html', response)

def activity (request):
    form = Activity_Form(request.POST or None)
    if(request.method == 'POST'):
        if(form.is_valid()):
            response['day'] = request.POST.get("day")
            response['date'] = request.POST.get("date")
            response['time'] = request.POST.get("time")
            response['eventname'] = request.POST.get("eventname")
            response['place'] = request.POST.get("place")
            response['category'] = request.POST.get("category")
            sched = Activity(day=response['day'], date=response['date'], time=response['time'], eventname=response['eventname'],place=response['place'], category=response['category'])
            sched.save()
            schedule = Activity.objects.all()
            response['schedule'] = schedule
            return render(request, 'Story3(activity).html', response)
        else:
            return render(request, 'Story3(createactivity).html', response)
    else:
        response['form'] = form
        return render(request, 'Story3(activity).html', response)

def delete(request):
    schedule = Activity.objects.all().delete()
    response['schedule'] = schedule
    return render(request, 'Story3(activity).html', response)

        


