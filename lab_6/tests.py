# from django.test import TestCase

# # Create your tests here.
# from django.test import Client
# from .models import Status
# from .forms import Status_Form
# from django.urls import resolve
# from django.http import HttpRequest
# import unittest
# from .views import landingPage
# from .views import profile
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import unittest
# import time

# class Lab6_Test(TestCase):
#     def test_lab_6_url_is_exist(self):
#         response = Client().get('/lab_6/')
#         self.assertEqual(response.status_code, 200)

#     def test_lab_6pro_url_is_exist(self):
#         response = Client().get('/lab_6/profile')
#         self.assertEqual(response.status_code, 200)

#     def test_model_can_creat_new_status(self):
#         new_status = Status.objects.create(status="currently pursuing Computer Science Degree")

#         counting_all_status = Status.objects.all().count()
#         self.assertEqual(counting_all_status, 1)

#     def test_lab6_using_create_form_func(self):
#         found= resolve('/lab_6/')
#         self.assertEqual(found.func, landingPage)

#     def test_lab_6_html_text(self):
#         request = HttpRequest()
#         response = landingPage(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<h1 class="monFont">Hello apa kabar?</h1>', html_response)

#     def test_fullname_text(self):
#         request = HttpRequest()
#         response = profile(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Full Name :Alaya Khairunnisa<br></p>', html_response)

#     def test_name_text(self):
#         request = HttpRequest()
#         response = profile(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Nick Name :Alaya<br></p>', html_response)

#     def test_date_of_birth_text(self):
#         request = HttpRequest()
#         response = profile(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Date of Birth :January 19th 2000<br></p>', html_response)

#     def test_age_text(self):
#         request = HttpRequest()
#         response = profile(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Age :18<br></p>', html_response)

#     def test_place_of_birth_text(self):
#         request = HttpRequest()
#         response = profile(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Place of Birth :Jakarta<br></p>', html_response)

#     def test_address_text(self):
#         request = HttpRequest()
#         response = profile(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Address :Mawar Raya Street<br></p>', html_response)

#     def test_hobbies_text(self):
#         request = HttpRequest()
#         response = profile(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Hobbies :Reading, Cooking, and<br>Watching<br></p>', html_response)

#     def test_siblings_text(self):
#         request = HttpRequest()
#         response = profile(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<p>Siblings :2 brothers and 2 sister<br></p>', html_response)

# class Lab6_functionaltest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(Lab6_functionaltest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab6_functionaltest, self).tearDown()

#     def test_status(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/')
#         time.sleep(3)
#         status_box = selenium.find_element_by_class_name('form-control')
#         status_box.send_keys('Coba Coba')
#         status_box.submit()
#         assert 'Coba Coba' in selenium.page_source

#     def test_check_title_landing_page(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/')
#         assert 'Landing Page' in selenium.title

#     def test_check_title_profile(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/profile')
#         assert 'Profile' in selenium.title

#     def test_css_button(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/')
#         button = selenium.find_element_by_tag_name("button")
#         self.assertIn('submit', button.get_attribute("type"))

#     def test_button(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/lab_6/')
#         button = selenium.find_element_by_tag_name("a")
#         self.assertIn('btn', button.get_attribute("class"))
        

#     # def test_check_class_row(self):
#     #     selenium.find_element_by_class_name('row')




    

    