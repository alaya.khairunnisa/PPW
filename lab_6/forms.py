from django import forms
from .models import Status

class Status_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    status = forms.CharField(label = 'status', widget=forms.Textarea(attrs=attrs), required=True)

