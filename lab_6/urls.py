from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.landingPage, name = 'landingPage'),
    path('profile', views.profile, name = 'profile',)
    # path('statusDisplay', views.statusDisplay, name = 'statusDisplay')
]