from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import Status_Form
from .models import Status

response={}
# Create your views here.
def landingPage (request):
    form = Status_Form(request.POST or None)
    response = {}
    allviewstatus = Status.objects.all()
    response = {
        "allviewstatus" : allviewstatus
    }
    response['form'] = form
    if(request.method == 'POST' and form.is_valid()):
            status = request.POST.get("status")
            Status.objects.create(status = status)
            return redirect('landingPage')
    else :
        return render(request, 'landingPage.html', response)
    
def profile (request):
    return render(request, "profile.html", {})
# def statusDisplay(request):
#     response = {}
#     allviewstatus = Status.objects.all()
#     response = {
#         "allviewstatus" : allviewstatus
#     }
#     return render(request, 'landingPage.html', response)
