from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
import requests
import json

# Create your views here.
response={}
def data(request, category):
    txt = request.GET.get('q')
    json_list = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + category).json()
    json_parse = json.dumps(json_list)
    return HttpResponse(json_parse)

def index(request):
    if request.user.is_authenticated and "counter" not in request.session:
        request.session['user'] = request.user.username 
        request.session['email'] = request.user.email
        request.session['sessionId'] = request.session.session_key
        request.session['counter'] = []
    return render(request, 'lab_9.html')

@csrf_exempt
def add(request):
    if(request.method == "POST"):
        favs = request.session["counter"]
        if request.POST["id"] not in favs:
            favs.append(request.POST["id"])
        print(request.session['sessionId'])
        print(request.session['counter'])
        request.session['counter'] = favs
        response["message"] = len(favs)
        return JsonResponse(response)
    else:
        return HttpResponse("Method not allowed")

@csrf_exempt
def mins(request):
    if(request.method == "POST"):
        favs = request.session["counter"]
        if request.POST["id"] in favs:
            favs.remove(request.POST["id"])
        print(request.session['sessionId'])
        print(request.session["counter"])
        request.session["counter"] = favs
        response["message"] = len(favs)
        return JsonResponse(response)
    else:
        return HttpResponse("Method not allowed")

def getFav(request):
    if request.user.is_authenticated:
        if(request.method == "GET"):
            if request.session["counter"] is not None:
                response["message"] = request.session["counter"]
            else:
                response["message"] = "NOT ALLOWED"
        else:
            response["message"] = ""
        return JsonResponse(response)







