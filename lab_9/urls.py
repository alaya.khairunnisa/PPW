from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.contrib.auth import views
from .views import index
from .views import data
from .views import add
from .views import mins
from .views import getFav

app_name = "lab9"
urlpatterns = [
    path('', index, name = 'lab_9'),
    path('data/<str:category>', data, name = 'data'),
    path('add/', add, name="add"), 
    path('min/', mins, name="mins"),
    path('getFav/', getFav, name="getFav"),
]