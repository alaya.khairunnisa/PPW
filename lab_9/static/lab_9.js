var category = "quilting";
$(document).ready(function() {
    checkFav();
    $(".buttonq").on("click", function(){
        var category = $(this).text();
        $.ajax({
            url: "data/" + category,
            datatype: 'json',
            success: function(data){
                var obj = jQuery.parseJSON(data)
                renderHTML(obj);
            },
            error: function(error){
                alert("Books not found");
            },
        })
    });

    $(".buttona").on("click", function(){
        var category = $(this).text();
        $.ajax({
            url: "data/" + category,
            datatype: 'json',
            success: function(data){
                var obj = jQuery.parseJSON(data)
                renderHTML(obj);
            },
            error: function(error){
                alert("Books not found");
            }
        })
    });

    $(".buttonc").on("click", function(){
        var category = $(this).text();
        $.ajax({
            url: "data/" + category,
            datatype: 'json',
            success: function(data){
                var obj = jQuery.parseJSON(data)
                renderHTML(obj);
            },
            error: function(error){
                alert("Books not found");
            }
        })
    });
    $.ajax({
        url: "data/" + category,
        datatype: 'json',
        success: function(data){
            var obj = jQuery.parseJSON(data)
            renderHTML(obj);
        },
        error: function(error){
            alert("Books not found");
        }
    });

	
var container = document.getElementById("demo");
function renderHTML(data){
    htmlstring = "<tbody>";
    $("tbody").text("");
    console.log(data);
	for(i = 0; i < data.items.length;i++){
		htmlstring += "<tr>"+
		"<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
		// "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
		"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
		"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favourite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
	} 
    container.insertAdjacentHTML('beforeend', htmlstring+"</tbody>")
    checkFav();
}
});

var counter = 0;
function checkFav(){
    $.ajax({
        type: 'GET',
        url: '/lab_9/getFav/',
        datatype : 'json',
        success: function(data){
            for(var i=1; i <= data.message.length; i++){
                console.log(data.message[i-1]);
                var id = data.message[i-1];
                var td = document.getElementById(id);
                if(td!=null){
                    td.className = 'clicked';
                    td.src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
                }
                $('#counter').html(data.message.length);
            }
        }

    });
};

function favourite(id){
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var user = document.getElementById(id);
    var liked = 'https://image.flaticon.com/icons/svg/291/291205.svg';
    var unliked = 'https://image.flaticon.com/icons/svg/149/149222.svg';
    if(user.className == 'checked'){
        $.ajax({
            url: "/lab_9/min/",
            type: "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data:{
                id: id,
            },
            success: function(result){
                counter = result.message;
                user.className = '';
                user.src = unliked;
                $('#counter').html(counter);
            },
            error: function(errorMessage){
                alert("Something is wrong");
            }
        });
    }else{
        $.ajax({
            url: "/lab_9/add/",
            type: "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data:{
                id: id,
            },
            success: function(result){
                console.log(user);
                counter = result.message;
                user.className = 'checked';
                user.src = liked;
                $('#counter').html(counter);
            },
            error: function(errorMessage){
                alert("Something is wrong like");
            }
        })
    }
}

