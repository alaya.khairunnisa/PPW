from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options 
from .views import index
import unittest
import time

class Lab_9_Test(TestCase):
    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab_9/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_exists(self):
        response = Client().get('/lab_9/data/quilting')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_using_template(self):
        response = Client().get('/lab_9/')
        self.assertTemplateUsed(response, 'lab_9.html')

    def test_lab_9_using_index_func(self):
        found = resolve('/lab_9/')
        self.assertEqual(found.func, index)

    # def test_lab_9_index_header(self):
    #     request = HttpRequest()
    #     response = index(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Top 10 Books', html_response)

class Lab_9_FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(Lab_9_FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab_9_FunctionalTest, self).tearDown()

    def test_check_title(self):
        selenium = self.selenium
        selenium.get('http://ppw-app-alaya-f.herokuapp.com/lab_9/')
        self.assertIn('Favorites', selenium.title)

    # def test_css(self):
    #     selenium = self.selenium
    #     selenium.get('http://ppw-app-alaya-f.herokuapp.com/lab_9/')
    #     content = selenium.find_element_by_tag_name('p').value_of_css_property('color')
    #     self.assertIn('rgba(100, 100, 100, 1)', content)
    #     time.sleep(2)




    
