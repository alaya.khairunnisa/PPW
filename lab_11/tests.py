from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
import unittest


# Create your tests here.
class finalUnitTest(TestCase):
    def test_landing_url_exist(self):
        response = Client().get('http://ppw-app-alaya-f.herokuapp.com/lab_11/')
        self.assertEqual(response.status_code, 200)
    
    def test_landing_using_index_func(self):
        found = resolve('/lab_11/')
        self.assertEqual(found.func, landing)

    def test_landing_html_template(self):
        response = Client().get('/lab_11/')
        self.assertTemplateUsed(response, 'landing.html')
    
    def test_landing_url_logout_exist(self):
        response = Client().get('http://ppw-app-alaya-f.herokuapp.com/lab_11/logout/')
        self.assertEqual(response.status_code, 200)

