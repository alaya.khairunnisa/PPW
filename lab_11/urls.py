from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import include
from django.contrib.auth import views
from .views import landing, logout_view

urlpatterns = [
    path('', landing, name='landing'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', logout_view, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')), # <- Here
]