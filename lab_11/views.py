from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import logout
import json

# Create your views here.
response = {}
def landing(request):
    return render(request, 'landing.html', response)

def logout_view(request):
    request.session.flush()
    logout(request)
    return render(request, 'landing.html', {})

